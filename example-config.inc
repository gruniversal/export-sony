#!/bin/bash

##
## Konfiguration
## 
## Es werden zunächst die Standard-Werte aus der defaults.lib eingelesen
## und danach die eigentliche Konfigurationsdatei. Demnach müssen hier
## nur Werte angegeben werden, die sich vom Standard unterscheiden.
## 

## Pfade ##

# Mountpunkt des Media-Players
#PATH_MNT=""

# Quellverzeichnis: dieses wird auf den Media-Player übertragen
#PATH_SRC=""

# Zielverzeichnis: dorthin werden die Daten übertragen
#PATH_DST=""

# temporäres Verzeichnis
#PATH_TMP=""

# Logverzeichnis: hier werden Logs für jeden Export abgelegt
#PATH_LOG=""

# Diese Pfade werden beim Export nicht mit übertragen
#CONF_EXCLUDES[0]=""
