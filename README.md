# Export Sony

This bash script exports the personal music library on the Sony Walkman NWZ-15A MP3 player.

I made the script after simply copying of the files resulted in a false order of tracks within an album.

Later I added a functionality to embed reduced cover images in the mp3 files. These are displayed by the player.

Author:

* David Gruner, https://www.gruniversal.de

License:

* Creative Commons BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/

## What it does ##

* check configuration and requirements
* check if MP3 player is mounted
* read your music library (you can exclude paths in the configuration)
* copy each mp3 file to remove id3 track information (this caused the false order)
* convert cover images and embed them in the mp3 file
* write the new mp3 file to the MP3 player (skips existing files)
* display progress on screen and log everything into a log file

## How to use ##

* copy example-config.inc to config.inc
* configure config.inc to your needs
* run the script in a bash shell

## Configuration ##

The script uses a default configuration in `defaults.lib` which probably suffice your needs.

You can setup you own configuration in `config.inc`:

**Mount path**
* PATH_MNT - default: `"/media/$USER_NAME/SONYWALKMAN"`

**Source path** (this directory will be transferred)
* PATH_SRC - default: `$XDG_MUSIC_DIR`

**Destination path** (on Sony device)
* PATH_DST - default: `"$PATH_MNT/MUSIC"`

**Temporary path**
* PATH_TMP - default: `"/home/$USER_NAME/.export_sony_sh/tmp"`

**Logfiles path**
* PATH_LOG - default: `"/home/$USER_NAME/.export_sony_sh/log"`

**Excludes** (list of directories within $PATH_SRC that are excluded)
* CONF_EXCLUDES[0] - default: `""`

## Requirements ##

* bash
* eyeD3
* convert (from imagemagick)

## Important Notes ##

**Warnings**

* all outputs and code comments are in german (started it this way, maybe I change this later)
* the current code is very straightforward and mainly based on my personal needs

**Cover generation**

* the script only looks for cover images named `cover.jpg` in the same directory
* if it finds these, it converts them to a reduced version:`cover_240px.jpg`
* these reduced cover images stay in your local music library permanently

**Compatibility**

* I have not tested this script for other Sony Walkman devices of the NWZ-series
* probably this will also work and if, please let me know :)

## Roadmap ##

None. It works for my needs.

## Changelog ##

### v0.4 (20.03.2021) ###

**Changes**

* improved id3 tag handling:
  * remove id3v1 tags to ensure correct play order for tracks
  * convert tags to id3v2.3 (since the player does not recognize id3v2.4)
  * thanks to: https://ubuntuforums.org/showthread.php?t=2035559
* removed id3v2 requirement:
  * now uses eyeD3 in one step to change mp3 files (which makes it also a bit faster)

### v0.3 (23.04.2020) ###

**Changes**

* first public release
* moved default configuration to `defaults.lib`
* intelligent recognition of personal music folder
* improved check if all necessary folders are present and readable
* more code readability

### v0.2 (2018) ###

**Changes**

* feature to exclude paths
* replace invalid characters in filenames (e.g. question marks)
* check requirements at startup
* skip existing files
* indicate cover creation
* fixed cover transfer (it finally works correct!)
* cleaned logging a bit
* improved code readability

### v0.1 (2017) ###

Initial version
