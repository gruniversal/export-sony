#!/bin/bash
#
# export_sony.sh: Exportiert das persönliche Musikarchiv auf den MP3-Player Sony NWZ-15A
#
# Parameter:
# 
# keine
#
#=================================================================================

##
## Konfiguration
##
## Es wird zunächst die defaults.lib eingelesen und danach die
## eigentliche Konfigurationsdatei. Dort müssen demnach nur Werte
## angegeben werden, die sich vom Standard unterscheiden.
## 

CONF_FILE="config.inc";

# ------------------------------------------------- #
#  Ab hier keine weitere Anpassungen erforderlich!  #
# ------------------------------------------------- #

## Startup ##

# Uninitialisierte Variablen melden
set -o nounset

# Script-Pfad ermitteln
STARTUP_PATH="${BASH_SOURCE%/*}"
if [[ ! -d "${STARTUP_PATH}" ]]; then STARTUP_PATH="${PWD}"; fi

# Standard-Library für Hilfsfunktionen einbinden
source "${STARTUP_PATH}/common.lib"

# Infoteil
echo_copyright "export_sony: Exportscript Sony NWZ-A15 (c) 2017-2021 gruniversal.de"

echo "Dieses Script exportiert die persönliche Musiksammlung auf den MP3-Player Sony NWZ-A15."
echo

# Requirements prüfen
assert_requirement "eyeD3"
assert_requirement "convert"

# Standard-Konfiguration einlesen
source "${STARTUP_PATH}/defaults.lib"

# gewählte Konfiguration einlesen - überschreibt ggf. Standards
source "${STARTUP_PATH}/${CONF_FILE}"

## Prüfe Verzeichnisse ##

# Temp-Verzeichnis

if [[ ! -d "${PATH_TMP}" ]]; then
  echo_warn "Das temporäre Verzeichnis '${PATH_TMP}' wurde nicht gefunden. Lege es automatisch an."
  mkdir -p "${PATH_TMP}" || true
  echo  
  exit $EXIT_DEVICE_ERROR
fi

if [[ ! -w "${PATH_TMP}" ]]; then
  echo_fail "Das temporäre Verzeichnis '${PATH_TMP}' ist nicht beschreibbar."
  echo  
  exit $EXIT_DEVICE_ERROR
fi

FILE_LST="${PATH_TMP}/list"
echo_info "Es wird folgende Dateiliste genutzt: ${FILE_LST}"

# Log-Verzeichnis

if [[ ! -d "${PATH_LOG}" ]]; then
  echo_warn "Das Log-Verzeichnis '${PATH_LOG}' wurde nicht gefunden. Lege es automatisch an."
  mkdir -p "${PATH_LOG}" || true
  echo  
  exit $EXIT_DEVICE_ERROR
fi

if [[ ! -w "${PATH_LOG}" ]]; then
  echo_fail "Das Log-Verzeichnis '${PATH_LOG}' ist nicht beschreibbar."
  echo  
  exit $EXIT_DEVICE_ERROR
fi

DATE_LOG=`date +%Y%m%d_%H%M%S`
FILE_LOG="${PATH_LOG}/${DATE_LOG}.txt"

# Mount-Punkt für MP3-Player

check_mounted "$PATH_MNT"
if (($? == "0")); then
  echo "Der MP3-Player muss für den Export gemountet werden."
  echo_warn "Bitte den MP3-Player als '${PATH_MNT}' mounten."
  echo  
  exit $EXIT_DEVICE_ERROR
fi

echo_good "Der MP3-Player ist unter '${PATH_MNT}' gemountet."
echo_warn "Der Player darf während des Exports nicht entfernt werden."
echo
echo "Quelle = ${PATH_SRC}"
echo "Export = ${PATH_DST}"
echo

# Quellverzeichnis

if [[ ! -d "${PATH_SRC}" ]]; then
  echo_fail "Das Quellverzeichnis '${PATH_SRC}' wurde nicht gefunden."
  echo  
  exit $EXIT_DEVICE_ERROR
fi

# Zielverzeichnis

if [[ ! -d "${PATH_DST}" ]]; then
  echo_fail "Das Zielverzeichnis '${PATH_DST}' wurde nicht gefunden."
  echo  
  exit $EXIT_DEVICE_ERROR
fi

# Ausgeschlossene Verzeichnisse

if ((${#CONF_EXCLUDES[*]} > "0")); then
  echo "Folgende Ordner werden ausgelassen:"
  for VAR_EXCLUDE in ${CONF_EXCLUDES[*]}
  do
    echo -e "${COL_YELLOW}${VAR_EXCLUDE}${COL_RESET}"
  done
  echo
fi

## Sicherheitsabfrage ##

echo -ne "${MARK_ASK} Export beginnen [JN]: "

while [[ forever ]]; do
  read -sn1 VAR_ANSWER
  case "${VAR_ANSWER}" in

    [Nn])
      echo -e "${COL_RED}Abbruch.${COL_RESET}"
      echo
      exit $EXIT_ABORTED_BY_USER
    ;;

    [JjYy])
      echo
      echo
      break
    ;;

  esac
done

echo
echo

## Point of no Return ##

#exit

## Export zum MP3-Player ##

# Arbeitspfad setzen
cd "${PATH_SRC}"

# Log-Funktionen
function log {
  echo $1 >> ${FILE_LOG}
}
function log-time {
  echo -n $1 >> ${FILE_LOG}
  date >> ${FILE_LOG}
}

# Log starten
log "[export_sony.sh]"
log-time "Verarbeitung begann: "

echo "Logfile: ${FILE_LOG}"
echo

# Step 1: Liste aller MP3s erstellen
log "Ermittle Dateiliste: ${FILE_LST}"
find -name "*.mp3" > ${FILE_LST}

# CONF_EXCLUDES ausnehmen (via grep)
for VAR_EXCLUDE in ${CONF_EXCLUDES[*]}
do
  cat ${FILE_LST} | grep -v "${VAR_EXCLUDE}" > ${FILE_LST}_new
  mv ${FILE_LST}_new ${FILE_LST}
done

# Step 2: Anzahl der MP3s ermitteln und Counter einrichten
VAR_WC1=`wc -l ${FILE_LST}`
VAR_WC2=`expr index "${VAR_WC1}" " "`
VAR_WC3=${VAR_WC1:0:$VAR_WC2-1}
log "Anzahl der Dateien: >${VAR_WC3}<"
log ""
log "src: ${PATH_SRC}/"
log "tmp: ${PATH_TMP}/"
log "dst: ${PATH_DST}/"

VAR_CUR=0
VAR_MAX=$((VAR_WC3))

# Für Erkennung Verzeichniswechsel
VAR_PATH_OLD=""

# Step 3: Liste der Dateien durchlaufen
while read VAR_LINE
do

  # Dateiname und Pfad ermitteln
  LEN_LINE=${#VAR_LINE}
  VAR_BASE=`basename "${VAR_LINE}"`
  LEN_BASE=${#VAR_BASE}
  LEN_PATH=$((LEN_LINE-LEN_BASE-1))
  VAR_PATH=${VAR_LINE:0:$LEN_PATH}

  # Ungültige Zeichen in Dateiname (auf Ziellaufwerk) ersetzen, aktuell folgende: ?:
  echo ${VAR_BASE} > ${PATH_TMP}/sedfile
  sed -i 's/:/ /g' ${PATH_TMP}/sedfile
  sed -i 's/?/ /g' ${PATH_TMP}/sedfile
  VAR_BASE=`cat ${PATH_TMP}/sedfile`

  # absolute Pfade definieren
  FILE_SRC="${PATH_SRC}/${VAR_LINE}"
  FILE_TMP="${PATH_TMP}/${VAR_BASE}"
  FILE_DST="${PATH_DST}/${VAR_PATH}/${VAR_BASE}"

  ## Verzeichnis-Operationen ##

  # Verzeichniswechsel erkennen
  if [ "${VAR_PATH}" != "${VAR_PATH_OLD}" ]; then
    log ""
    log "${VAR_PATH}"
    VAR_PATH_OLD="${VAR_PATH}"

    ##
    ## Erstellung Cover-Thumbnail
    ##
    ## Der Player zeigt nach meiner Recherche keine jpgs direkt an, sondern erwartet das Cover
    ## eingebunden in der MP3 als ID3-Tag. Hierbei darf eine bestimmte Größe nicht überschritten
    ## werden und das jpg darf nicht interlaced (sondern nur baseline) sein. Die entsprechende
    ## Konvertierung erfolgt über Imagemagick (per convert).
    ## 

    # Suche nach Album-Cover (muss "cover.jpg" heißen und im gleichen Ordner liegen)
    FILE_CVR="${VAR_PATH}/cover.jpg"
    if [ -f "${FILE_CVR}" ]; then

      # Check ob Cover-Thumbnail vorhanden, notfalls erzeugen
      FILE_TMB="${VAR_PATH}/cover_${SIZE_TMB}px.jpg"
      if [ ! -f "${FILE_TMB}" ]; then
        log " -> Cover-Thumbnail wird neu erzeugt: ${FILE_TMB}"
        convert "${FILE_CVR}" -interlace none -resize ${SIZE_TMB}x${SIZE_TMB} "${FILE_TMB}"

        echo -n "C"
      fi

      ##
      ## Album-Cover kopieren
      ## 
      ## Obwohl es vom Player nicht direkt angezeigt wird, übertrage ich hier trotzdem das Thumbnail.
      ## Zum einen dient das der Kontrolle, zum anderen ist es nützlich, falls man mal Ordner direkt
      ## vom MP3-Player aus kopieren möchte.
      ## 
      
      # Thumbnail als cover.jpg zum Zielort bewegen (ggf. Zielverzeichnis anlegen)
      FILE_CVR_TMP="${PATH_TMP}/cover.jpg"
      cp "${FILE_TMB}" "${FILE_CVR_TMP}"      
      mkdir -p "${PATH_DST}/${VAR_PATH}"
      mv "${FILE_CVR_TMP}" "${PATH_DST}/${VAR_PATH}"
    fi

    # hier ggf. weitere Operationen für Verzeichnisse hinzufügen ..

  fi

  ## Datei-Operationen ##

  # überspringe Dateien in Verzeichnissen mit Datei .skip_export_sony
  FILE_SKP="${VAR_PATH}/.skip_export_sony"
  if [ -f "${FILE_SKP}" ]; then
    log " SKIP: ${VAR_BASE}"
    echo -n "S"

  else

    log " => ${VAR_BASE}"

    # bestehende Dateien überspringen (spart deutlich Zeit und verhindert Neuindexierung)
    if [ -f "${FILE_DST}" ]; then

      echo -n "."

    else

      # Tempdatei erstellen
      cp "${FILE_SRC}" "${FILE_TMP}"

      # Anpassung der MP3 für Sony Walkman
      # - bei ID3v1 wird Tracknummer ggf. falsch erkannt
      # - nutze daher ID3v2.3 Tags (v2.4 versteht der Player nicht)
      # - wenn vorhanden: Thumbnail in MP3 integrieren (für Coverdisplay)
      # (Dank an: https://ubuntuforums.org/showthread.php?t=2035559)    
      FILE_TMB="${VAR_PATH}/cover_${SIZE_TMB}px.jpg"
      if [ -f "${FILE_TMB}" ]; then
        eyeD3 --to-v2.3 --add-image="${FILE_TMB}":FRONT_COVER "${FILE_TMP}" >/dev/null 2>&1
      else
        eyeD3 --to-v2.3 "${FILE_TMP}" >/dev/null 2>&1
      fi

      # hier ggf. weitere Operationen für MP3s hinzufügen ..

      # Zielverzeichnis anlegen und zum Zielort bewegen
      mkdir -p "${PATH_DST}/${VAR_PATH}"
      mv "${FILE_TMP}" "${PATH_DST}/${VAR_PATH}"

      echo -n "#"

    fi

  fi

  # Counter erhöhen
  ((VAR_CUR++))
  VAR_MOD=`expr ${VAR_CUR} % 50`

  if ((${VAR_MOD}==0)); then
    echo " ${VAR_CUR}/${VAR_MAX}"
  fi

  if ((${VAR_CUR}==${VAR_MAX})); then
    echo " ${VAR_CUR}/${VAR_MAX}"
  fi

done < ${FILE_LST}

# Log abschließen
log ""
log-time "Verarbeitung beendet: "

echo
